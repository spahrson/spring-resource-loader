package ch.spahrson.example.springconfig;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@SpringBootApplication
public class App {

    public static void main(String[] args) throws IOException {

        ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(App.class)
               .build().run(args);


        CustomResourceLoader customResourceLoader = (CustomResourceLoader) applicationContext.getBean("customResourceLoader");

        Resource resource = customResourceLoader.getResource();
        System.out.println("Resource '" + resource + "' contains:");
        print(resource.getInputStream());

    }

    public static void print(InputStream is) throws IOException {
        String readLine;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        while (((readLine = br.readLine()) != null)) {
            System.out.println(readLine);
        }
    }

}

